simple-char-count
=================

JQyuery Plugin - Simple Character Counter

requires JQuery

usage:

<form>
<textarea id="message"></textarea>
</form>

<script type="text/javascript">
$(function(){
    $("#message").charCounter({"max": 250});
});
</script>
